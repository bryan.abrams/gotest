package main

import (
  "io"
  "fmt"
  opentracing "github.com/opentracing/opentracing-go"
  jaeger "github.com/uber/jaeger-client-go"
  config "github.com/uber/jaeger-client-go/config"
)

func InitializeJaeger() (opentracing.Tracer, io.Closer) {
  cfg := &config.Configuration {
    Sampler: &config.SamplerConfig {
      Type: "const",
      Param: 1,
    },
    Reporter: &config.ReporterConfig {
      LogSpans: true,
    },
  }
  tracer, closer, err := cfg.New("api", config.Logger(jaeger.StdLogger))
  if err != nil {
    panic(fmt.Sprintf("ERR: %v", err))
  }
  return tracer, closer
}