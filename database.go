package main

import (
  "time"
  "context"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
  "go.mongodb.org/mongo-driver/mongo/readpref"
)

func InitializeDatabase(uri string) *mongo.Client {
  context, _ := context.WithTimeout(context.Background(), 10 * time.Second)
  client, _ := mongo.Connect(context, options.Client().ApplyURI(uri))
  return client
}

func PingDatabase(client *mongo.Client) bool {
  context, _ := context.WithTimeout(context.Background(), 2 * time.Second)
  err := client.Ping(context, readpref.Primary())
  if err == nil {
    return true
  }
  return false
}