package main

import "fmt"

func main() {
  database := InitializeDatabase("mongodb://localhost")
  http := InitializeHttp()

  dbRunning := PingDatabase(database)
  if dbRunning == true {
    StartHttp(http)
  }
  fmt.Println("You shouldn't be seeing this.")
}
